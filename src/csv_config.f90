!*******************************************************************************
!>
!  Various config.

module csv_config_m

    use, intrinsic :: iso_fortran_env, only: dp => real64, sp => real32, ip => int32
    implicit none
    private

    public :: max_integer_str_len, max_real_str_len, default_int_fmt, default_real_fmt, sp_real_fmt

    integer(ip), parameter :: max_real_str_len = 27 !! maximum string length of a real number
    character(*), parameter :: default_real_fmt = '(es24.16e3)'
        !! default real number format statement (for writing real values to strings and files).
    character(*), parameter :: sp_real_fmt = "(es15.8e2)"

    integer(ip), parameter :: max_integer_str_len = 256 !! maximum string length of an integer.
    character(*), parameter :: default_int_fmt = '(I256)'
        !! default integer number format statement (for writing real values to strings and files).

end module csv_config_m
!*******************************************************************************
