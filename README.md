# Parser for CSV-files in Fortran

A personality collection of commonly used functions for CSV-files for Fortran.

*Suggestions and code contributions are welcome.*

```fortran
use csv_fortran_m, only: csv_file
```

## Build with [fortran-lang/fpm](https://github.com/fortran-lang/fpm)

```sh
fpm run --example --list
```

```toml
[dependencies]
csv-fortran = { git = "https://gitee.com/zoziha/csv-fortran.git" }
```

## Reference

- [jacobwilliams/fortran-csv-module](https://github.com/jacobwilliams/fortran-csv-module)
